/*
 * bonobo-glibwww-control.c
 *
 * Author:
 *    Ariel Rios (ariel@arcavia.com)
 * 
 * Copyright 2000 Ariel Rios
 */

#include <config.h>
#include <gnome.h>
#include <glibwww.h>
#include <bonobo.h>

#include "bonobo-glibwww-control.h"

void
bonobo_glibwww_factory_init (void)
{
	static BonoboGenericFactory *bonobo_glibwww_control_factory = NULL;

	if (bonobo_calc_control_factory != NULL)
		return;
/*
#if USING_OAF
	bonobo_glibwww_control_factory =
		bonobo_generic_factory_new (
		        "OAFIID:bonobo_glibwww_factory: ",
			bonobo_glibwww_factory, NULL);

#else
        bonobo_glibwww_control_factory =
                bonobo_generic_factory_new (
		        "control-factory:glibwww",
			bonobo_glibwww_factory, NULL);
#endif
      
        if (bonobo_glibwww_control_factory == NULL)
                g_error ("I could not register a BonoboGlibwww factory.");
}
