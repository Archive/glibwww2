/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
#ifdef HAVE_CONFIG_H
#include "config.h"
#endif
#include <string.h>
#include <gnome.h>
#include "glibwww.h"

#define DEFAULT_OUTPUT_FILE "get.out"

static void terminate_handler(const gchar *url, const gchar *file,
			      int status, gpointer user_data)
{
  g_message("Downloaded '%s' to '%s' with return code %d", url, file, status);
  gtk_main_quit();
}

int main(int argc, char **argv)
{
  char *outputfile = DEFAULT_OUTPUT_FILE;
  char *getme = NULL;
  int arg;

  gnome_init(PACKAGE, VERSION, argc, argv);

  glibwww_init(PACKAGE, VERSION);
  glibwww_register_gnome_dialogs();

  for (arg = 1; arg < argc; arg++) {
    if (!strcmp(argv[arg], "-o")) {
      outputfile = (arg+1 < argc && *argv[arg+1] != '-') ?
	argv[++arg] : DEFAULT_OUTPUT_FILE;
    } else {
      getme = argv[arg];
    }
  }

  if (getme && *getme) {
    GWWWRequest *request;

    request = glibwww_load_to_file(getme, outputfile, terminate_handler,
				   NULL);
    if (request) {
      gtk_main();
    }
  } else
    g_printerr("Usage: %s <address> -o <localfile>\n", argv[0]);

  return 0;
}

